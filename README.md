﻿# Ristretto Transmission

This notebook is a reference document to keep track of transmission during the project.

To launch the notebook in the browser : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.unige.ch%2Fristretto%2Fristretto-transmission/HEAD?labpath=RISTRETTO_transmission.ipynb)

Beware nothing is saved there, just for experimentation, the lauch can be quite slow (10-15 minutes) when the notebook is run after an update of the repository.

* To clone it on your machine :

in a parent directory : 
```
git clone https://gitlab.unige.ch/ristretto/ristretto-transmission.git
```

Some other way to do.

```
cd existing_repo
git remote add origin https://gitlab.unige.ch/ristretto/ristretto-transmission.git
git branch -M main
git push -uf origin main
```

